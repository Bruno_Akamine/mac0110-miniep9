function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end
function matrix_pot(M,p)
    if length(M)==1
        resp=[1]
        for i in 1:p
            resp[1]*=M[1]
        end
        return resp
    end
    resp=M
    for i in 1:p-1
        resp=multiplica(resp,M)
    end
    return resp
end
function matrix_pot_by_squaring(M,p)
    if length(M)==1
        resp=[1]
        for i in 1:p
            resp[1]*=M[1]
        end
        return resp
    end
    identidade=Matrix{Number}(undef,size(M)[1],size(M)[2])
    for i in 1:size(M)[1]
        for j in 1:size(M)[2]
            if i==j
                identidade[i,j]=1
            else
                identidade[i,j]=0
            end
        end
    end
    if p==1
        return M
    end
    if p==0
        return identidade
    end 
    if p%2==0
        resp=matrix_pot_by_squaring(multiplica(M,M),p/2)
    end
    if p%2==1
        resp=multiplica(M,matrix_pot_by_squaring(multiplica(M,M),(p-1)/2))
    end
    return resp
end

using LinearAlgebra
using Test
function teste()
    K=Matrix(LinearAlgebra.I,50,50)
    for i in 100:100:1000
        @test matrix_pot(K,i)==K
        @test matrix_pot_by_squaring(K,i)==K
    end
    @test matrix_pot(K,100)==K
    @test matrix_pot_by_squaring(K,100)==K
    @test matrix_pot([1 2 ; 3 4], 1)==[1 2;3 4]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 1)==[1 2;3 4]
    @test matrix_pot([1 2;3 4],2)==[7.0 10.0; 15.0 22.0]
    @test matrix_pot_by_squaring([1 2;3 4],2)==[7.0 10.0; 15.0 22.0]
    @test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)==[5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8; 8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8; 5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8; 7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]
    @test matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)==[5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8; 8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8; 5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8; 7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]
    println("Fim dos testes")
end
function compare_times()
    M= Matrix(LinearAlgebra.I, 30, 30)
    A=Matrix(LinearAlgebra.I,100,100)
    for i in 100:100:1000
        println("Testando tempos da matriz identidade 100x100 elevado a ",i)
        @time matrix_pot(A,i)
        @time matrix_pot_by_squaring(A,i)
    end
end
teste()